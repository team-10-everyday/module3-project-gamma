import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from "react";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react"
import Pokemon from './Pokemon';
import IndividualPokemon from './IndividualPokemonPage';
import OwnedPokemon from './OwnedPokemon';
import ListTypes from './Types';
import Login from './Login';
import SignUp from './SignUp'
import MainPage from './MainPage';
import UserInfo from './User'
import CreateTeam from './CreateTeam';
import Teams from './Teams'
import EditTeam from './EditTeam';
import CreateDuel from './CreateDuel';
import IndividualDuel from './IndividualDuel';
import EditUser from './EditUser';
import Nav from './Nav';
import "./App.css";
import IndividualType from './IndividualTypePage';


function App() {
  const [launchInfo, setLaunchInfo] = useState([]);
  const [error, setError] = useState(null);


  useEffect(() => {
    async function getData() {
      let url = `${process.env.REACT_APP_API_HOST}/api/launch-details`;
      console.log("fastapi url: ", url);
      let response = await fetch(url);
      console.log("------- hello? -------");
      let data = await response.json();

      if (data.ok) {
        console.log("got launch data!");
        setLaunchInfo(launchInfo);
      } else {
        console.log("drat! something happened");
        setError(error);
      }
    }
    getData()
  }, [launchInfo, error]);

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '')

  return (
    <>
      <BrowserRouter basename={basename}>
        <AuthProvider baseUrl={`${process.env.REACT_APP_API_HOST}`}>
          <Nav />
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path="/user/protected" element={<UserInfo />} />
            <Route path="/pokemon" element={<Pokemon />} />
            <Route path="/user/pokemon/protected" element={<OwnedPokemon />} />
            <Route path="/pokemon/:id" element={<IndividualPokemon />} />
            <Route path="/types" element={<ListTypes />} />
            <Route path="/types/:id" element={<IndividualType />} />
            <Route path="/user/teams/protected" element={<Teams />} />
            <Route path="/teams" element={<CreateTeam />} />
            <Route path="/teams/:id" element={<EditTeam />} />
            <Route path="/duels/:id" element={<IndividualDuel />} />
            <Route path="/user/:id" element={<EditUser />} />
            <Route path="/duels" element={<CreateDuel />} />
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </>
  );
}


export default App;
