import useToken from "@galvanize-inc/jwtdown-for-react";
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function MainPage() {
  const [user, setUser] = useState("");
  const navigate = useNavigate();
  const { token } = useToken();

  useEffect(() => {
    if (token) {
      const getUserData = async () => {
        const url = `${process.env.REACT_APP_API_HOST}/user/protected`;
        try {
          const response = await fetch(url, {
            credentials: "include",
          });

          if (response.ok) {
            const data = await response.json();
            setUser(data);
          };
        } catch (error) {
          console.error(error);
        }
      };

      getUserData();
    }
  }, [token]);


  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="title-sim display-3 fw-bold">Pocket Simulator</h1>
      <div className="greeting">
        {token && (
          <>
            <h2 className="display-8 fw-bold"> Welcome back, {user.username} 👋</h2>
            <button className="duel-btn btn-dark btn-lg" style={{ width: '200px' }} onClick={() => navigate('/duels')}>
              Duel!
            </button>
          </>
        )}
        {!token && (
          <>
            <div className="signup-login-container">
              <h2 className="trainer-new display-8 fw-bold"> Hello, trainer! 👋</h2>
              <button className="btn btn-secondary btn-lg me-4" onClick={() => navigate('/signup')}>
                Signup
              </button>
              <button className="btn btn-dark btn-lg" onClick={() => navigate('/login')}>
                Login
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default MainPage;
