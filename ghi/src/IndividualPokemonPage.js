import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function IndividualPokemon() {
  const [pokemon, setPokemon] = useState("");
  const { id } = useParams()

  useEffect(() => {
    const getData = async () => {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/pokemon/${id}`);
      if (response.ok) {
        const data = await response.json();
        setPokemon(data);
      }
    }
    getData();
  }, [id]);


  return (
    <div className="col-poke">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>{pokemon.name}</h1>
          <img src={pokemon.picture} alt={pokemon.name} />
          <h5>ID</h5>
          <p>{pokemon.id}</p>
          <h5>Type</h5>
          <p>{pokemon.type}</p>
          <h5>Strengths</h5>
          <p>{pokemon.strengths}</p>
          <h5>Weaknesses</h5>
          <p>{pokemon.weaknesses}</p>
          <h5>Description</h5>
          <p>{pokemon.description}</p>
        </div>
      </div>
    </div>
  );

}

export default IndividualPokemon;
