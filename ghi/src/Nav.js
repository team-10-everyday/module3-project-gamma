import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import './App.css';

const Nav = () => {
  const { logout, token } = useToken();
  const navigate = useNavigate();
  const handleLogout = (e) => {
    e.preventDefault();
    logout()
    navigate("/");
  }


  return (
    <nav className="navbar navbar-expand-lg">
      <div className="container-fluid">
        <div className="navbar-brand-spacing">
          <div className="container-fluid"></div>
          <NavLink className="navbar-brand" to="/">
            Home
          </NavLink>
          <NavLink className="navbar-brand" to="/pokemon">
            Pokemon
          </NavLink>
          <NavLink className="navbar-brand" to="/types">
            Types
          </NavLink>
        </div>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
          <div className="collapse navbar-collapse" id="navbarSupportedContent"></div>
          {token && (
            <>
              <li className="nav-item">
                <NavLink className="navbar-brand" to="/user/protected">
                  Profile
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="navbar-brand" to="/user/pokemon/protected">
                  My Pokemon
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="navbar-brand" to="/user/teams/protected">
                  Teams
                </NavLink>
              </li>
              <li className="nav-item">
                <button className="btn btn-secondary" onClick={handleLogout}>
                  Logout <i className="bi bi-box-arrow-left"></i>
                </button>
              </li>
            </>
          )}
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
