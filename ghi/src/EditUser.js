import React, { useState, useEffect } from 'react';
import { useNavigate} from "react-router-dom";
import { useParams } from 'react-router-dom';

function EditUser() {
  const [username, setUsername] = useState('');
  const navigate = useNavigate();
  const { id } = useParams();

  async function handleSubmit(event) {
    event.preventDefault();
    const UserData = {
      username: username,
    };
    const userurl = `${process.env.REACT_APP_API_HOST}/users/${id}`
    const patchConfig = {
      method: "put",
      body: JSON.stringify(UserData),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const response = await fetch(userurl, patchConfig
    );
    if (response.ok) {
      event.target.reset();
      navigate("/user/protected");
    }
  };


  useEffect(() => {
    setUsername()
  }, []);

  function handleChangeUsername(event) {
    const { value } = event.target;
    setUsername(value);
  }


  return (
    <div className="row-form">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Edit Username</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label className="form-label">User Name</label>
              <input
                value={username}
                onChange={handleChangeUsername}
                name="username"
                type="username"
                className="form-control"
              />
            </div>
            <div className="text-end">
              <input className="btn btn-dark" type="submit" value="Submit" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditUser;
