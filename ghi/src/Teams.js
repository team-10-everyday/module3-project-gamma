import React, { useEffect, useState } from 'react';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import './App.css';

function Teams() {
  const [teams, setTeams] = useState([]);
  const navigate = useNavigate();
  const { token } = useToken()

  if (!token) {
    navigate("/")
  }

  const editTeam = (team) => {
    navigate(`/teams/${team.id}`, { state: { ...team } });
  }

  const goToCreateTeam = () => {
    navigate("/teams");
  }

  const handleDelete = async (id, idx) => {
    if (window.confirm('Are you sure you want to delete this team?')) {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/teams/${id}`, { method: "delete" })
      console.log(response)
      if (response.ok) {
        const updatedTeams = teams.splice((idx, 1))
        setTeams(updatedTeams)
        navigate("/user/teams/protected")
      }
    }
  }


  const TeamCard = ({ team, idx }) => {
    return (
      <>
        <div className="team-card" onClick={() => editTeam(team)}>
          <h3 className="teamname">{team.teamname}</h3>
          <h6><img src={team.pokemon1picture} className="list-picture" alt={team.pokemon1picture} /> {team.pokemon1name}</h6>
        </div>
        <div className="delete text-end">
          <button type="button-delete" className="btn btn-outline-danger" onClick={() => handleDelete(team.id, idx)}>Delete</button>
        </div>
      </>
    );
  };


  useEffect(() => {
    const getData = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/teams/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setTeams(data);
        })
        .catch((error) => console.error(error));
    };
    getData()
  }, []);


  return (
    <div className="row-form">
      <div className="offset-3 col-6">
        <div className="shadow bg-white p-4 mt-4">
          <div className="team-header">
            <h2>Your Teams</h2>
            <div className="create-team text-left">
              <button type="button" className="btn btn-dark" onClick={goToCreateTeam}>Add Team +</button>
            </div>
          </div>
          <div className="container">
            {teams.map((team, idx) => (
              <TeamCard className="team-card" key={team.id} team={team} idx={idx} />
            ))}
          </div>
          <div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default Teams;
