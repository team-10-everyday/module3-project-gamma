import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [invalid, setInvalid] = useState(false);
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const result = await login(username, password)
    if (result) {
      navigate("/")
      e.target.reset();
    } else {
      setInvalid(true)
      e.target.reset()
    }
  };

  return (
    <div className="row-form">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Login ↘️</h1>
          <form onSubmit={(e) => handleSubmit(e)}>
            <div className="mb-3">
              <label className="form-label">Username:</label>
              <input
                name="username"
                type="text"
                className="form-control"
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password:</label>
              <input
                name="password"
                type="password"
                className="form-control"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="text-end">
              <input className="btn btn-dark" type="submit" value="Login" />
            </div>
          </form>
          {invalid && (
            <div className="error-msg alert alert-secondary" role="alert">
              Invalid username or password 🛑
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Login;
