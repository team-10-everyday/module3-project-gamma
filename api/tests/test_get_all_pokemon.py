from fastapi.testclient import TestClient
from main import app
from query.pokemon import PokemonRepository

client = TestClient(app)


class EmptyPokemonRepository:
    def get_list_pokemon(self):
        return [{
            "id": 1,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png",
            "name": "Bulbasaur",
            "type": "Grass",
            "description": "A grass type Pokémon."
        },
         {
            "id": 2,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/212.png",
            "name": "Scizor",
            "type": "Steel",
            "description": "A Steel type Pokémon."
        },
        ]


def test_get_all_pokemon():
    app.dependency_overrides[PokemonRepository] = EmptyPokemonRepository

    response = client.get("/pokemon")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [{
            "id": 1,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png",
            "name": "Bulbasaur",
            "type": "Grass",
            "description": "A grass type Pokémon."
        },
        {
            "id": 2,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/212.png",
            "name": "Scizor",
            "type": "Steel",
            "description": "A Steel type Pokémon."
        },
        ]
