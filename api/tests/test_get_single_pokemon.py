from fastapi.testclient import TestClient
from main import app
from query.pokemon import PokemonRepository

client = TestClient(app)


class EmptyPokemonRepository:
    def get_pokemon(self, pokemon_id: int):
        return {
            "id": 2,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/212.png",
            "name": "Snorlax",
            "type": "Normal",
            "strengths": "None",
            "weaknesses": "Fighting",
            "description": "A normal type Pokémon."
        }


def test_get_single_pokemon():
    app.dependency_overrides[PokemonRepository] = EmptyPokemonRepository

    response = client.get("/pokemon/2")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
            "id": 2,
            "picture":
            "https://assets.pokemon.com/assets/cms2/img/pokedex/full/212.png",
            "name": "Snorlax",
            "type": "Normal",
            "strengths": "None",
            "weaknesses": "Fighting",
            "description": "A normal type Pokémon."
        }
