from fastapi import (APIRouter,
                     Depends,
                     Response,
                     HTTPException,
                     Request,
                     status)
from typing import List, Optional, Union
from query.users import (
    Error,
    UserIn,
    UserOut,
    TeamsOut,
    ListOwnedPokemonOut,
    UserRepository,
    DuplicateUserError,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from query.duels import DuelRepository, DuelsOut
from pydantic import BaseModel


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UserOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/users", response_model=AccountToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UserRepository = Depends(),
):
    hashedpassword = authenticator.hash_password(info.password)
    print("here hashed_password", hashedpassword)
    print("here")
    try:
        print("trying")
        account = repo.create_user(info, hashedpassword)
        print("account from create method", account)
        print("done trying")
    except DuplicateUserError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    print("here we are nowww")
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    print("token", token)
    print(account)
    return AccountToken(account=account, **token.dict())


@router.get("/user/protected", response_model=Optional[UserOut])
def get_user(
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Optional[UserOut]:
    if not account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
        )
    return account


@router.put("/users/{user_id}", response_model=Optional[UserOut])
def update_username(
    user_id: int,
    repo: UserRepository = Depends(),
) -> Optional[UserOut]:
    user = repo.update_username(user_id)
    return user


@router.get(
    "/user/pokemon/protected",
    response_model=Union[List[ListOwnedPokemonOut], Error],
)
def get_pokemon_by_user(
    repo: UserRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[List[ListOwnedPokemonOut], Error]:
    if not account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
        )
    user_owned_pokemon = repo.get_pokemon_by_user(account["username"])
    return user_owned_pokemon


@router.get("/user/duels/protected", response_model=Union[
    List[DuelsOut], Error
    ])
def get_duels(
    account: dict = Depends(authenticator.try_get_current_account_data),
    repo: DuelRepository = Depends(),
) -> Union[List[DuelsOut], Error]:
    if not account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Unauthorized"
        )
    duels = repo.get_duels(account["username"])
    return duels


@router.get("/user/teams/protected",
            response_model=Union[List[TeamsOut], Error])
def get_teams(
    repo: UserRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data)
) -> Union[List[TeamsOut], Error]:
    teams = repo.get_teams(account["id"])
    return teams
