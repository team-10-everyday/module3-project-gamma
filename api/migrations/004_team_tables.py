steps = [
    [
        """
        CREATE TABLE teams (
            id SERIAL PRIMARY KEY NOT NULL,
            owner_id int,
            teamname VARCHAR(1000),
            pokemon1 int

        );
        """,
        """
        DROP TABLE teams;
        """
    ],
]
