import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from query.users import UserRepository, UserOutWithPassword


class ExampleAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: UserRepository,
    ):
        return accounts.get_user(username)

    def get_account_getter(
        self,
        accounts: UserRepository = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: UserOutWithPassword):
        print("DICT", account)
        return account["hashedpassword"]

    def get_account_data_for_cookie(self, account: UserOutWithPassword):
        return account["username"], UserOutWithPassword(**account)


authenticator = ExampleAuthenticator(os.environ["SIGNING_KEY"])
