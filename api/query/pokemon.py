from pydantic import BaseModel
from query.pool import pool
from typing import List, Optional


class Error(BaseModel):
    message: str


class PokemonIn(BaseModel):
    picture: str
    name: str
    type: str
    description: str


class PokemonOut(BaseModel):
    id: int
    picture: str
    name: str
    type: str
    description: str


class PokemonSingleOut(BaseModel):
    id: int
    picture: str
    name: str
    type: str
    strengths: str
    weaknesses: str
    description: str


class PokemonRepository:
    def get_pokemon_by_type(self, type: str) -> List[PokemonOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT pokemon.id,
                        pokemon.name,
                        pokemon.picture,
                        pokemon.type,
                        pokemon.description,
                        types.strengths,
                        types.weaknesses
                        FROM pokemon
                        INNER JOIN types ON pokemon.type = types.typename
                        WHERE type = %s
                        ORDER BY pokemon.name
                        """,
                        [type],
                    )
                    return [
                        self.record_to_pokemon_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not pokemon by type"}

    def get_pokemon(self, pokemon_id: int) -> Optional[PokemonSingleOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT  pokemon.id,
                        picture,
                        name,
                        type,
                        types.strengths,
                        types.weaknesses,
                        description
                        FROM pokemon
                        JOIN types on type = types.typename
                        WHERE pokemon.id = %s
                        """,
                        [pokemon_id],
                    )
                    record = result.fetchone()
                    print(record)
                    if record is None:
                        return None
                    return self.record_to_pokemonsingle_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that pokemon"}

    def get_list_pokemon(self) -> List[PokemonOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, picture, name, type, description
                        FROM pokemon
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_pokemon_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all pokemon"}

    def record_to_pokemon_out(self, record):
        return PokemonOut(
            id=record[0],
            picture=record[1],
            name=record[2],
            type=record[3],
            description=record[4],
        )

    def record_to_pokemonsingle_out(self, record):
        return PokemonSingleOut(
            id=record[0],
            picture=record[1],
            name=record[2],
            type=record[3],
            strengths=record[4],
            weaknesses=record[5],
            description=record[6],
        )
