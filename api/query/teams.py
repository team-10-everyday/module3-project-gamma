from pydantic import BaseModel
from query.pool import pool
from typing import Union


class Error(BaseModel):
    message: str


class TeamIn(BaseModel):
    teamname: str
    pokemon1: int


class TeamInUpdate(BaseModel):
    teamname: str
    pokemon1: int


class TeamOut(BaseModel):
    id: int
    owner_id: int
    teamname: str
    pokemon1: int


class TeamRepository:
    def update_team(self, team_id: int, team: TeamIn) -> Union[TeamIn, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE teams
                        SET teamname = %s,
                        pokemon1 = %s
                        WHERE id = %s
                        """,
                        [team.teamname, team.pokemon1, team_id],
                    )
                    return self.teamupdate_in_to_out(team_id, team)
        except Exception as e:
            print(e)
            return {"message": "Could not update that team"}

    def create_team(
        self, team: TeamIn, owner_id: int
    ) -> Union[TeamOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO teams
                            (teamname, pokemon1, owner_id)
                        VALUES
                            (%s, %s, %s)
                         RETURNING id;
                        """,
                        [team.teamname, team.pokemon1, owner_id],
                    )
                    id = result.fetchone()[0]
                    response = self.team_in_to_out(id, team, owner_id)
                    return response
        except Exception as e:
            print({e})
            return {"message": "Create did not work"}

    def delete(self, team_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM teams
                        WHERE id = %s
                        """,
                        [team_id],
                    )
                    return True
        except Exception:
            return False

    def team_in_to_out(self, id: int, team: TeamIn, owner_id: int):
        return TeamOut(
            id=id,
            teamname=team.teamname,
            pokemon1=team.pokemon1,
            owner_id=owner_id,
        )

    def teamupdate_in_to_out(self, team_id: int, team: TeamIn):
        return TeamIn(
            id=team_id, teamname=team.teamname, pokemon1=team.pokemon1
        )

    def record_to_team_out(self, record):
        return TeamOut(
            id=record[3],
            teamname=record[2],
            pokemon1=record[4],
            owner_id=record[5],
        )
